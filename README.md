#     adv≡nture  
###    Bottom text
---

Some text adventure '''game''' I'm making in python

Still very basic and in work

The clear screen function may not work in Windows or Python env :P

Tbh it is more of a template thing to build the actual game on top, so if you want to use it as a base, go ahead.

##### What's working (gameplay wise) of now:

* Move around rooms
* Look around, at inventory or specific object
* Read objects with text (like signs or letters)
* pick objects from ground and drop from inventory (weight system)
* interact with certain objects (interactible objects can have special functions assigned)
* Get attacked or attack enemies (the combat is basically: every attack has a min/max damage it can give and the damage is random :P)
* ↑ Get killed/Kill 
* Unlock doors
* Open containers like chests
* Dialogue tree

Feel free to tell me how I'm doing everything wrong and there are numbers of ways I could be doing something better :P

_so professional, lol_